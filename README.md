# Overview

This is an example simple webapp that's automatically deployed to an AWC ECS cluster ... of 1 EC2 instance (to stay in the free tier).

The HTTP server code lives in the `./test-http` folder. It's built using Docker Hub Automated builds, so the docker image lives in my [Docker Hub][docker-hub-automated-builds].

The code in the `./deploy` folder uses the AWS SDK to automatically create everything required and then runs the http server via docker on the AWS ECS cluster.

[docker-hub-automated-builds]: https://hub.docker.com/r/duboisf/cloud-test/builds/

# Disclaimer

This whole thing might not even work... I sure hope it does though!

# Prerequisites

  - An AWS account (you _might_ want to create a new AWS account so this project doesn't screw something up)
  - An AWS user that has Admin priveleges (see [AWS ECS documentation][aws-ecs-documentation] for how to set this up)
  - Probably need a *default* AWS VPC, because, uh, I didn't feel like creating one.
  - A linux machine (I used Linux Mint but any Ubuntu/Debian derivative should work). You can probably run this on a Mac or even Windows, but I can't help you here.
  - Your AWS credentials saved in your home directory, see below for how.
  - Some installed packages, namely npm and node (8.x). See below for how to install node 8.x.

[aws-ecs-documentation]: http://docs.aws.amazon.com/AmazonECS/latest/developerguide/get-set-up-for-amazon-ecs.html#create-an-iam-user

## Install node 8.x

```shell
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
```
## Setup your AWS credentials in ~/.aws

I installed the `awscli` package and used `aws configure` to setup my aws credentials on my local machine.

# Deploy the http server

Just run the `run-deploy` script in the root of the git repo.

Note: there will probably be some errors while running but the script will do retries so just let it retry, it should go through.

It seems some things aren't useable immediately (like instance profiles, also the EC2 instance takes a bit of time to register with the cluster) but I didn't see any way to wait for them to be ready to use, so I implemented a retry mechanism for those.

# "Advanced" usage

```shell
cd deploy
npm install
npm run build
npm run deploy -- [--delete] INSTANCE_NAME
WHERE
    --delete        deletes the EC2 instance INSTANCE_NAME and everything related to it
    INSTANCE_NAME   EC2 instance name
```
