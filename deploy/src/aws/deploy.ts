import { EC2, ECS } from 'aws-sdk';
import { CommonEc2 } from './common';
import * as fs from 'fs';
import * as os from 'os';
import * as path from 'path';
import * as colors from 'colors';

const logWithArrows = (msg: string) =>
    console.info(green('-->  ') + msg + green('  <--'));

const sshTemplate = (keyPath: string, publicHostname: string) =>
    `ssh -o "StrictHostKeyChecking no" -i ${keyPath} ec2-user@${publicHostname}`

const sleepInSeconds = (timeInSeconds: number) =>
    new Promise(resolve => setTimeout(resolve, timeInSeconds * 1000));

const green = colors.green;
const yellow = colors.yellow;
const red = colors.red;

async function retryIfError<T>(func: () => Promise<T>, waitInSeconds: number = 5): Promise<T> {
    try {
        return await func();
    } catch (err) {
        const errType = err && err.code ? `error '${red(err.code)}'` : red('error');
        console.warn(`Got unexpected ${errType}! ${green('Retrying')} in ${waitInSeconds} seconds...`);
        await sleepInSeconds(waitInSeconds);
        return retryIfError(func, waitInSeconds);
    }
}

export class Deploy extends CommonEc2 {
    
    private createKeyPair() {
        this.logCreating('key');
        return this.ec2.createKeyPair({ KeyName: this.keyName })
            .promise()
            .catch(this.ignoreError('InvalidKeyPair.Duplicate'));
    }

    private createSecurityGroup() {
        this.logCreating('security group');
        return this.ec2.createSecurityGroup({
            GroupName: this.securityGroupName,
            Description: `Security group for ${this.instanceName}`
        }).promise().catch(this.ignoreError('InvalidGroup.Duplicate'));
    }

    private async createServiceRole() {
        this.logCreating('service role');
        await this.iam.createRole({
            RoleName: this.serviceRoleName,
            AssumeRolePolicyDocument: JSON.stringify({
                "Version": "2008-10-17",
                "Statement": [
                    {
                    "Sid": "",
                    "Effect": "Allow",
                    "Principal": {
                        "Service": "ecs.amazonaws.com"
                    },
                    "Action": "sts:AssumeRole"
                    }
                ]
            })
        }).promise().catch(this.ignoreExistsError);
        console.info('Attach service role to policy');
        return this.iam.attachRolePolicy({
            RoleName: this.serviceRoleName,
            PolicyArn: this.policyEcs
        }).promise();
    }

    private async createInstanceRole() {
        this.logCreating('instance role');
        await this.iam.createRole({
            RoleName: this.instanceRoleName,
            AssumeRolePolicyDocument: JSON.stringify({
                "Version": "2008-10-17",
                "Statement": [
                    {
                        "Sid": "",
                        "Effect": "Allow",
                        "Principal": {
                            "Service": [
                                "ec2.amazonaws.com",
                                "ecs.amazonaws.com"
                            ]
                        },
                        "Action": "sts:AssumeRole"
                    }
                ]
            })
        }).promise().catch(this.ignoreExistsError);
        this.logCreating('instance profile');
        await this.iam.createInstanceProfile({
            InstanceProfileName: this.instanceProfileName
        }).promise().catch(this.ignoreExistsError);
        console.info('Add role to instance profile');
        await this.iam.addRoleToInstanceProfile({
            InstanceProfileName: this.instanceProfileName,
            RoleName: this.instanceRoleName,
        }).promise().catch(this.ignoreError('LimitExceeded'));
        console.info('Attach instance role to policy');
        return this.iam.attachRolePolicy({
            RoleName: this.instanceRoleName,
            PolicyArn: this.policyEcsForEc2
        }).promise();
    }

    private async createRoles() {
        await this.createServiceRole();
        return this.createInstanceRole();
    }

    private createCluster() {
        this.logCreating('cluster');
        return this.ecs.createCluster({
            clusterName: this.clusterName
        }).promise().catch(this.ignoreExistsError);
    }

    private async runInstance(groupId: string): Promise<EC2.Instance> {
        const maybeInstance = await this.findEc2InstanceByName();
        if (maybeInstance) return maybeInstance;
        const searchResult = await this.ec2.describeInstances({
            Filters: [
                { Name: 'tag:Name', Values: [this.instanceName] },
                { Name: 'instance-state-name', Values: ['pending', 'running']}
            ]
        }).promise();
        if (searchResult.Reservations && searchResult.Reservations.length) {
            const insts = searchResult.Reservations[0].Instances;
            if (insts && insts.length) {
                console.info(`Found existing instance with name tag name ${this.instanceName}!`);
                return insts[0];
            }
        }
        const base64EncodedUserData = new Buffer([
            '#!/bin/bash',
            `echo ECS_CLUSTER=${this.clusterName} >> /etc/ecs/ecs.config`
        ].join('\n')).toString('base64');
        const res = await retryIfError(() => {
            this.logYellow('Try', 'to create instance');
            return this.ec2.runInstances({
                ImageId: "ami-04351e12", // Amazon ECS optimized for us-east-1
                SecurityGroupIds: [groupId],
                MaxCount: 1,
                MinCount: 1,
                TagSpecifications: [{
                    ResourceType: 'instance',
                    Tags: [{ Key: 'Name', Value: this.instanceName }]
                }],
                InstanceType: 't2.micro',
                IamInstanceProfile: { Name: this.instanceProfileName },
                UserData: base64EncodedUserData,
                KeyName: this.keyName
            }).promise().catch(this.ignoreExistsError);
        });
        if (res && res.Instances && res.Instances.length) {
            return res.Instances[0];
        }
        throw new Error('No instance information available');
    }

    private async registerTask() {
        console.info('Registering task');
        return this.ecs.registerTaskDefinition({
            containerDefinitions: [
                {
                    name: this.instanceName,
                    cpu: 10,
                    essential: true,
                    image: this.imageName,
                    memory: 500,
                    portMappings: [{
                        hostPort: 80,
                        containerPort: 8080
                    }]
                }
            ],
            family: this.taskFamily
        }).promise().catch(this.ignoreExistsError);
    }    

    private async runTask() {
        this.logYellow('Try', 'to run task...');
        return this.ecs.runTask({
            cluster: this.clusterName,
            taskDefinition: this.taskFamily
        }).promise();
    }

    private readonly privateKeyFolder = path.join(os.homedir(), '.ec2_keys');

    private readonly privateKeyPath = path.join(this.privateKeyFolder, `${this.instanceName}-key.pem`);

    private createPrivateKeyFolder(): void {
        if (!fs.existsSync(this.privateKeyFolder)) {
            fs.mkdirSync(this.privateKeyFolder);
            fs.chmodSync(this.privateKeyFolder, 0o700);
        }
    }

    private writeKeyFile(keyContent: string): void {
        this.createPrivateKeyFolder();
        if (fs.existsSync(this.privateKeyPath)) {
            this.logYellow('Warning:', `key file ${this.privateKeyPath} already exists, overwriting`);
            fs.unlinkSync(this.privateKeyPath);
        }
        this.logGreen('Saving', 'ssh key to ' + this.privateKeyPath);
        fs.writeFileSync(this.privateKeyPath, keyContent, { mode: 0o400 });
    }

    private readonly describeInstance = (instanceId: string) =>
        this.ec2.describeInstances({ InstanceIds: [instanceId] }).promise();

    private waitForInstanceToBeRunning(instanceId: string) {
        console.info('Waiting for instance to be running...');
        return this.ec2.waitFor('instanceRunning', { InstanceIds: [instanceId] }).promise();
    }

    private waitForTaskRunning(taskId: string) {
        console.info(`Waiting for task ${taskId} to be running...`);
        return this.ecs.waitFor('tasksRunning', {
            tasks: [taskId],
            cluster: this.clusterName
        }).promise();
    }

    private getPublicDnsFrom(result: EC2.Types.DescribeInstancesResult): string | null {
        if (!(result.Reservations && result.Reservations.length)) return null;
        const reservation = result.Reservations[0];
        if (!(reservation.Instances && reservation.Instances.length)) return null;
        const instance = reservation.Instances[0];
        if (!instance.PublicDnsName) return null;
        return instance.PublicDnsName;
    }

    private getTaskId(result: ECS.Types.RunTaskResponse): string | null {
        if (!(result.tasks && result.tasks.length)) return null;
        const task = result.tasks[0];
        if (!task.taskArn) return null;
        const parts = task.taskArn.split('/');
        return parts.length == 2 ? parts[1] : null;
    }

    private async createAuthGroupIngresses(groupId: string) {
        const authGroupTcpIngress = this.authSecGroupIngress(groupId)('tcp')
        this.logCreating('ssh permission');
        await authGroupTcpIngress(22);
        this.logCreating('http permission');
        await authGroupTcpIngress(80);
    }

    private async getSecurityGroupId(): Promise<string | null> {
        const res = await this.ec2.describeSecurityGroups({
            GroupNames: [this.securityGroupName]
        }).promise();
        if (res.SecurityGroups && res.SecurityGroups.length) {
            return res.SecurityGroups[0].GroupId || null;
        }
        return null;
    }    

    private async describeCluster() {
        const res = await this.ecs.describeClusters({
            clusters: [this.clusterName]
        }).promise();
        if (res.clusters && res.clusters.length) return res.clusters[0];
        else return null;
    }

    private waitForClusterActive(): Promise<void> {
        console.info('Waiting for cluster to be active...');
        return new Promise(async resolve => {
            let res = await this.describeCluster();
            while (res && res.status !== 'ACTIVE') {
                await sleepInSeconds(10);
                console.info('Cluster ' + yellow('not ready') + ' yet...');
                res = await this.describeCluster();
            }
            resolve();
        })
    }

    async run(): Promise<void> {
        try {
            const key = await this.createKeyPair();
            if (key && key.KeyMaterial) {
                this.writeKeyFile(key.KeyMaterial);
            }
            await this.createSecurityGroup();
            const groupId = await this.getSecurityGroupId();
            if (!groupId) throw new Error('Could not get group id');
            await this.createAuthGroupIngresses(groupId);
            await this.createRoles();
            await this.createCluster();
            const instance = await this.runInstance(groupId);
            if (!instance.InstanceId) throw new Error('Could not get instance id');
            await this.waitForInstanceToBeRunning(instance.InstanceId);
            console.info('Created new instance with id ' + instance.InstanceId);
            await this.registerTask();
            await this.waitForClusterActive();
            const task = await retryIfError(() => this.runTask());
            const taskId = this.getTaskId(task);
            if (!taskId) throw new Error('Could not get task id');
            await this.waitForTaskRunning(taskId);
            const runningInstances = await this.describeInstance(instance.InstanceId);
            const publicDns = this.getPublicDnsFrom(runningInstances);
            if (!publicDns) throw new Error('Could not find public dns for instance');
            const sshCommand = sshTemplate(this.privateKeyPath, publicDns);
            console.info(`You can ${green('ssh')} to your new instance with:`);
            logWithArrows(sshCommand);
            console.info(`Done! You can ${green('access')} the service here:`);
            logWithArrows(`http://${publicDns}`);
        } catch (err) {
            console.error('Got unexpected error: ' + err);
        }
    }

}
