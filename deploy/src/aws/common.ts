import { config, EC2, ECS, IAM } from 'aws-sdk';
import { flatMap } from 'lodash';
import * as colors from 'colors';

config.update({region: 'us-east-1'});

function notTerminated(state?: EC2.InstanceState): boolean {
    return state !== undefined && state.Name !== 'terminated';
}

export abstract class CommonEc2 {
    
    protected readonly keyName = `${this.instanceName}-key`;

    protected readonly clusterName = `${this.instanceName}-cluster`;

    protected readonly securityGroupName = `${this.instanceName}-sg`;

    protected readonly taskFamily = `${this.instanceName}-task`;

    protected readonly instanceRoleName = `${this.instanceName}-instance-role`;

    protected readonly instanceProfileName = `${this.instanceName}-instance-profile`;

    protected readonly serviceRoleName = `${this.instanceName}-service-role`;

    protected readonly userName = `${this.instanceName}-user`;

    protected readonly policyEcsForEc2 = 
        'arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role';

    protected readonly policyEcs =
        'arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceRole';

    protected readonly ec2 = new EC2({ apiVersion: '2016-11-15' });

    protected readonly ecs = new ECS({ apiVersion: '2014-11-13' });

    protected readonly iam = new IAM({ apiVersion: '2010-05-08' });
    
    constructor(
        protected readonly instanceName: string,
        protected readonly imageName: string
    ) {}

    protected readonly ignoreError = (errorName: string) => (error: any) => {
        if (error && error.code && error.code === errorName) {
            console.info(`${colors.yellow('Ignoring')} ${errorName} error`);
            return null;
        } else throw error;
    }

    protected readonly logCreating = (what: string) =>
        console.info(colors.green('Creating') + ' ' + what);

    protected readonly logColor = (color: keyof colors.Color, coloredMsg: string, restOfMsg: string) =>
        console.info(colors[color](coloredMsg) + ' ' + restOfMsg);

    protected readonly logGreen = (greenStuff: string, restOfMsg: string) =>
        this.logColor('green', greenStuff, restOfMsg);

    protected readonly logYellow = (yellowStuff: string, restOfMsg: string) =>
        this.logColor('yellow', yellowStuff, restOfMsg);

    protected readonly logDeleting = (what: string) =>
        console.info(colors.red('Deleting') + ' ' + what);

    protected readonly ignoreExistsError = this.ignoreError('EntityAlreadyExists');

    protected readonly ignoreNoSuchEntityError = this.ignoreError('NoSuchEntity');

    protected readonly authSecGroupIngress = (groupId: string) => (protocol: string) => (port: number) =>
        this.ec2.authorizeSecurityGroupIngress({
            GroupId: groupId,
            IpProtocol: protocol,
            FromPort: port,
            ToPort: port,
            CidrIp: '0.0.0.0/0'
        }).promise().catch(this.ignoreError('InvalidPermission.Duplicate'));

    private simpleDescribeInstanceFilter(name: string, value: string): EC2.DescribeInstancesRequest {
        return { Filters: [{ Name: name, Values: [value] }] };
    }

    protected readonly filterByKeyName = () =>
        this.simpleDescribeInstanceFilter('key-name', this.keyName);

    protected async findEc2InstanceByName() {
        this.logGreen('Query', 'for existing instance with same tagged name');
        const searchResult = await this.ec2.describeInstances({
            Filters: [
                { Name: 'tag:Name', Values: [this.instanceName] },
                { Name: 'instance-state-name', Values: ['pending', 'running']}
            ]
        }).promise();
        if (searchResult.Reservations && searchResult.Reservations.length) {
            const insts = searchResult.Reservations[0].Instances;
            if (insts && insts.length) {
                this.logGreen('Found', `existing instance with name tag name ${this.instanceName}!`);
                return insts[0];
            }
        }
        return null;
    }

    protected async getInstanceIdsWithKey(): Promise<string[]> {
        const describeInstances = await this.ec2.describeInstances(this.filterByKeyName()).promise();
        const reservations = describeInstances.Reservations;
        if (reservations && reservations.length) {
            return flatMap(reservations, res =>
                flatMap(res.Instances, inst =>
                    (inst.InstanceId && notTerminated(inst.State)) ? [inst.InstanceId] : []));
        }
        return [];
    }

}