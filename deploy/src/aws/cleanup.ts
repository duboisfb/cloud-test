import { CommonEc2 } from './common';
import * as colors from 'colors';

export class Cleanup extends CommonEc2 {

    async run() {
        const log = console.info;
        const instance = await this.findEc2InstanceByName();
        if (instance && instance.InstanceId) {
            const id = instance.InstanceId;
            log('Terminating instance with id: ' + id);
            await this.ec2.terminateInstances({ InstanceIds: [id] }).promise();
            await this.ec2.waitFor('instanceTerminated', { InstanceIds: [id] }).promise()
        }
        this.logDeleting('cluster');
        await this.ecs.deleteCluster({ cluster: this.clusterName })
            .promise().catch(this.ignoreNoSuchEntityError)
        this.logDeleting('key ' + this.keyName);
        await this.ec2.deleteKeyPair({ KeyName: this.keyName }).promise();
        this.logDeleting('security group ' + this.securityGroupName);
        await this.ec2.deleteSecurityGroup({ GroupName: this.securityGroupName }).promise()
            .catch(this.ignoreError('InvalidGroup.NotFound'))
        this.logDeleting('role from instance profile');
        await this.iam.removeRoleFromInstanceProfile({
            InstanceProfileName: this.instanceProfileName,
            RoleName: this.instanceRoleName
        }).promise().catch(this.ignoreNoSuchEntityError)
        this.logDeleting('policy from role ' + this.instanceRoleName);
        await this.iam.detachRolePolicy({
            PolicyArn: this.policyEcsForEc2,
            RoleName: this.instanceRoleName
        }).promise().catch(this.ignoreNoSuchEntityError);
        this.logDeleting('instance profile ' + this.instanceProfileName );
        await this.iam.deleteInstanceProfile({ InstanceProfileName: this.instanceProfileName })
            .promise().catch(this.ignoreNoSuchEntityError);
        this.logDeleting('role ' + this.instanceRoleName );
        await this.iam.deleteRole({ RoleName: this.instanceRoleName })
            .promise().catch(this.ignoreNoSuchEntityError);
        this.logDeleting('policy from role ' + this.serviceRoleName);
        await this.iam.detachRolePolicy({
            PolicyArn: this.policyEcs,
            RoleName: this.serviceRoleName
        }).promise().catch(this.ignoreNoSuchEntityError);
        this.logDeleting('role ' + this.serviceRoleName );
        await this.iam.deleteRole({ RoleName: this.serviceRoleName })
            .promise().catch(this.ignoreNoSuchEntityError);
        log(colors.green('Done!'));
    }

}