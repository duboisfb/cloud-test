import * as process from 'process';
import * as path from 'path';
import * as minimist from 'minimist';
import { Deploy } from './aws/deploy';
import { Cleanup } from './aws/cleanup';


function usage(error?: string): never {
    const scriptName = path.basename(process.argv[1]);
    console.error(`USAGE: ${scriptName} [--delete] INSTANCE_NAME
WHERE
    --delete        deletes the EC2 instance INSTANCE_NAME and everything related to it
    INSTANCE_NAME   EC2 instance name

Without the --delete option, a new EC2 instance with the name INSTANCE_NAME is created.`)
    if (error) console.error('\nError: ' + error);
    return process.exit(1);
}

const ALPHANUMERIC_RE = /^[0-9a-zA-Z_-]+$/;

const parseArgs = () => minimist(process.argv.slice(2), { boolean: 'delete' });

try {
    const args = parseArgs();
    if (args._.length !== 1) {
        usage();
    } else {
        const instanceName = args._[0];
        if (!instanceName.match(ALPHANUMERIC_RE)) {
            usage('INSTANCE_NAME must be alphanumeric')
        }
        if (args.delete) {
            const cleanup = new Cleanup(instanceName, 'duboisf/cloud-test');
            cleanup.run();
        } else {
            const deploy = new Deploy(instanceName, 'duboisf/cloud-test');
            deploy.run()
        }
    }
} catch (err) {
    console.error('Unexpected error: ' + err);
}