import * as process from 'process';
import * as http from 'http';

function exitOnSignal(signal: any): void {
  process.on(signal, () => {
    console.warn('\ncaught ' + signal + ', exiting');
    process.exit(1);
  });
}

exitOnSignal('SIGINT');
exitOnSignal('SIGTERM');

const port = 8080;

const server = http.createServer((req, res) => {
  console.info('Received request at ' + new Date().toString());
  res.end('Hello AppDirect Platform')
});

server.listen(port, (err: any) => {
  if (err) {
    return console.info('Could not start server', err)
  }
  console.info(`server started up on port ${port}`)
});
